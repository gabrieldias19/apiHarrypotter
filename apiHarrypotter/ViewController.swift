//
//  ViewController.swift
//  apiHarrypotter
//
//  Created by COTEMIG on 20/10/22.
//

import UIKit
import Alamofire
import Kingfisher

struct Harry:Decodable {
    let name:String
    let actor:String
    let image:String
}

class ViewController: UIViewController,UITableViewDataSource {

    var listaDePersonagens: [Harry]=[]
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listaDePersonagens.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula",for: indexPath) as! MyCell
        let personagem = self.listaDePersonagens[indexPath.row]
        
        
        cell.nome_personagem.text = personagem.name
        cell.nome_autor.text = personagem.actor
        cell.img_personagem.kf.setImage(with: URL(string: personagem.image))
        return cell
    }
    
    @IBOutlet weak var tableViewPersonagens: UITableView!
    
    func getnovoharrypoter(){
        AF.request("https://hp-api.herokuapp.com/api/characters").responseDecodable(of: [Harry].self) { response in
            if let personagem = response.value {
                self.listaDePersonagens = personagem
                print(personagem)
            }
            self.tableViewPersonagens.reloadData()
        }
    }


    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.tableViewPersonagens.dataSource = self
        
        getnovoharrypoter()
    }
}
